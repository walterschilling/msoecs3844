/*
 * pipeDemo.c
 *
 *  Created on: Sep 20, 2016
 *      Author: W. Schilling
 *      This program will demonstrate the usage of pipes to send data from one process to another.
 */

#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char*argv[])
{
  /* The pipe file descriptors. */
  int p2cpipe[2];
  int c2ppipe[2];

  /* The text buffer that is to be used. */
  char text[255];

  /* the process id for the given set of processes. */
  pid_t pid;

  /* Initialize the pipes between processes. Note: We need two pipes because a pipe is unidirectional when defined this way.*/
  pipe(p2cpipe);
  pipe(c2ppipe);

  /* Fork the new processes. */
  pid = fork();

  if (pid < 0)
    {
      printf("Fork failed.\n");
      exit(-1);
    }
  else if (pid==0)
    {
      /* PID = 0.  Must be the slave process. */
      int index;
	  int offset;

	  // Initialize the buffer to be empty.
	  memset(&text[0], 0, 255);

	  // Close the pipes this process will not need.
	  close(p2cpipe[1]);
	  close(c2ppipe[0]);
      for (index = 0; index < 10; index++)
      {
		printf("C: Child is running.\n");

		/* Read from the pipe. */
        read(p2cpipe[0], &text[0], 255);

        /* Now that there is text, print it out. */
        printf("C: Slave Prints:>>>>%s\n", &text[0]);


		// Now that we are here, convert it to upper case.
		for (offset = 0; offset < 255; offset++)
		{
		  text[offset] = toupper(text[offset]);
		}

		// FLush the stdout so that things display on the screen.
		fflush(stdout);
		// Write the data into the pipe so that the parent receives it.
        write(c2ppipe[1], &text[0], 255);
      }
    }
  else{
    int index;
    // Initialize the buffer.
    memset(&text[0], 0, 255);

    // CLose the pipes we will not be needing.
	close(p2cpipe[0]);
	close(c2ppipe[1]);

    /* Must be the master process. */
    for (index = 0; index < 10; index++)
    {
      printf("M: Enter text to print.\n");
      fgets(&text[0], sizeof(text), stdin);

      /* Write to the pipe. */
      write(p2cpipe[1], &text[0], 255);
	  printf("M: Message sent to the child process.\n");
	  // Wait for the child process to respond to our message.
      read(c2ppipe[0], &text[0], 255);
	  printf("M: Message received from child process: %s\n", &text[0]);
    }
    /* Wait for the child process to complete. */
    waitpid(pid, NULL, 0);
  }
  return EXIT_SUCCESS;
}


