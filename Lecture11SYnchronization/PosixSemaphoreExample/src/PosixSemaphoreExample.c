/*
 ============================================================================
 Name        : PosixSemaphoreExample.c
 Author      : W. Schilling
 Version     :
 Description : This program demonstrates how one can use semaphores to synchronize
 two different threads.  In this example, the program is using
 binary semaphores.
 ============================================================================
 */
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#define LOOP_COUNT 1000000

static volatile long count = 0;

// Declare an instance of the semaphore that will be sued for synchronization.
static sem_t count_sem;

/*
 * This method will simply add a count to the index.  It does this by retrieving a temporary count value and adding 1 to it before storing it back into memory.
 */
void * ThreadAdd(void * a) {
	long i, tmp;
	for (i = 0; i < LOOP_COUNT; i++) {
		// Wait for access to the critical section.
		sem_wait(&count_sem);
		tmp = count; /* copy the global count locally */
		tmp = tmp + 1; /* increment the local copy */
		count = tmp; /* store the local value into the global count */
		sem_post(&count_sem);
		// Indicate we are done with the critical section.

		// Wait for access to the critical section.
		sem_wait(&count_sem);
		tmp = count;
		tmp = tmp + 1;
		count = tmp;
		sem_post(&count_sem);
		// Indicate we are done with the critical section.
	}
	return NULL;
}

int main(int argc, char * argv[]) {
	/*These are the two thread ids for the program. */
	pthread_t tid1, tid2;

	/* Initialize the semaphore.  The second integer indicates how many entries into the semaphore can occur before entry is blocked. */
	sem_init(&count_sem, 0, 1);

	/* Spawn the two threads. */
	if (pthread_create(&tid1, NULL, ThreadAdd, NULL) != 0) {
		printf("\n ERROR creating thread 1");
		exit(1);
	}

	if (pthread_create(&tid2, NULL, ThreadAdd, NULL) != 0) {
		printf("\n ERROR creating thread 2");
		exit(1);
	}

	/* Wait until the two threads are done. */
	if (pthread_join(tid1, NULL) != 0) /* wait for the thread 1 to finish */
	{
		printf("\n ERROR joining thread");
		exit(1);
	}

	if (pthread_join(tid2, NULL) != 0) /* wait for the thread 2 to finish */
	{
		printf("\n ERROR joining thread");
		exit(1);
	}

	long expected_count = LOOP_COUNT;
	expected_count *= 4;

	if (count != expected_count) {
		printf("\n BOOM! count is [%ld], should be %ld\n", count,
				expected_count);
	} else {
		printf("\n OK! count is [%ld]\n", count);
	}
	return EXIT_SUCCESS;

}
