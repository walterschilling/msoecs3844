/*
 ============================================================================
 Name        : posixMutexExample.c
 Author      : W. Schilling
 Version     :
 Description : This file demonstrates the usage of a POSIX Mutex for synchronization purposes.
 ============================================================================
 */
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>

#define LOOP_COUNT 1000000

static volatile long count = 0;

// Declare the mutex that will be used for synchronization.
static pthread_mutex_t mutex;

/*
 * This method will simply add a count to the index.  It does this by retrieving a temporary count value and adding 1 to it before storing it back into memory.
 */
void* ThreadAdd(void * a) {
	long i, tmp;
	for (i = 0; i < LOOP_COUNT; i++) {
		pthread_mutex_lock(&mutex);
		tmp = count; /* copy the global count locally */
		tmp = tmp + 1; /* increment the local copy */
		count = tmp; /* store the local value into the global count */
		pthread_mutex_unlock(&mutex);

		pthread_mutex_lock(&mutex);
		tmp = count;
		tmp = tmp + 1;
		count = tmp;
		pthread_mutex_unlock(&mutex);
	}
	return NULL;
}

int main(int argc, char * argv[]) {
	/*These are the two thread ids for the program. */
	pthread_t tid1, tid2;

	/* Create the mutex lock. The NULL indicates we will be using default attributes. */
	pthread_mutex_init(&mutex, NULL);

	/* Spawn the two threads. */
	if (pthread_create(&tid1, NULL, ThreadAdd, NULL) != 0) {
		printf("\n ERROR creating thread 1");
		exit(1);
	}

	if (pthread_create(&tid2, NULL, ThreadAdd, NULL) != 0) {
		printf("\n ERROR creating thread 2");
		exit(1);
	}

	/* Wait until the two threads are done. */
	if (pthread_join(tid1, NULL) != 0) /* wait for the thread 1 to finish */
	{
		printf("\n ERROR joining thread");
		exit(1);
	}

	if (pthread_join(tid2, NULL) != 0) /* wait for the thread 2 to finish */
	{
		printf("\n ERROR joining thread");
		exit(1);
	}

	long expected_count = LOOP_COUNT;
	expected_count *= 4;

	if (count != expected_count) {
		printf("\n BOOM! count is [%ld], should be %ld\n", count,
				expected_count);
	} else {
		printf("\n OK! count is [%ld]\n", count);
	}
	return EXIT_SUCCESS;
}
