#!/bin/bash

[ $# -eq 0 ] && { echo "Usage: $0 arg1"; exit 1; }
arg="$1"
while read -r line
do
    # Split the input file accordingly.
    IFS=, read -r f1 f2 <<<"$line"

    # Add the user to the system.
    useradd -m $f1 #-p $ph

    # Set the password accordingly.
    echo $f1:$f2 | chpasswd

done <"$arg"
