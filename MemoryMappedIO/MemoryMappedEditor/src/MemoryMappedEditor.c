/**
 * This program creates a simple editor using mmap to read in and edit files.
 *
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main (int argc, char *argv[])
{
	struct stat sb;
    off_t len;
    char *p;
    int fd;

    if (argc < 2) {
		fprintf (stderr, "usage: %s <file>\n", argv[0]);
		return 1;
    }

	// Obtain a file descriptor for a file opened as read write.
    fd = open (argv[1], O_RDWR);

	// If there is an error handle the error appropriately.
	if (fd == -1) {
		perror ("open");
		return 1;
    }

	// Check to see if the file exists.
	if (fstat (fd, &sb) == -1) {
      perror ("fstat");
      return 1;
    }

	// Make sure the given file is a file and not something else.
    if (!S_ISREG (sb.st_mode)) {
		fprintf (stderr, "%s is not a file\n", argv[1]);
		return 1;
    }

    // Create a map of the file into memory.  p will be a void pointer to the memory.
	p = mmap (0, sb.st_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

    if (p == MAP_FAILED) {
		perror ("mmap");
        return 1;
    }

	// Now close the given file.
	if (close (fd) == -1) {
		perror ("close");
        return 1;
    }

	int quit = 0;
	int offset = 0;

	while (quit == 0)
	{
	  // Do what the user asks you to do.
	  char action=' ';
	  char data=' ';

	  scanf("%c%c", &action, &data);

	  switch (action)
	  {
	    case 'q': // Quit the program
			quit = 1;
			break;
		case 'n': // Move to the next character.
			offset++;
			if (offset >=sb.st_size)
			{
			  offset = sb.st_size - 1;
			}
			printf("%c", p[offset]);
			break;
		case 'p': // Move to the previous character.
			offset--;
			if (offset <0)
			{
			  offset = 0;
			}
			printf("%c", p[offset]);
			break;
		case 'w': // Write a new charatcre into the location.
			p[offset] = data;
			printf("%c", p[offset]);
			// Remove the newline character from the stream...
			scanf("%c", &data);
			break;
		case 'd':
			printf("%c", p[offset]);
			break;
	  }
	}

	// Unmap the memory.
	if (munmap(p, sb.st_size) == -1) {
		perror ("munmap");
		return 1;
    }

	return 0;
}
