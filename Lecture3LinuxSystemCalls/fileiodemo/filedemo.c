#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE* fptr;  /* Declare a file pointer.  This will serve as a handle for the file that is to be opened. */

  printf("%s", argv[0]);  /* Print out arg[0] to the console.  This is the name of the program which is executing. */

  fptr = fopen(argv[1], "r"); /* Open the file.  Note this porgram is sloppy in that it does not check for a NULL return from this routine.  A NULL return would be indicitive of a non-existant file or other problems.   */

  while (!feof(fptr)) /* While we have not reached the end of the file, loop through the code. */
    {
      unsigned char text[255];   /* Declare an array of unsigned chars to hold the text we read in from the console. */
      fscanf(fptr, "%s", text);  /* Read in a string from the file. */
      printf("%s\n", text);      /* Print the file out to the console. */
    }
  fclose(fptr);                  /* Close the file. */
}
