SHELL = /bin/sh
SRCDIR = .
CC = gcc
YACC = bison -y
LIBS = 
CFLAGS = -g -save-temps -I. -I$(SRCDIR)
LDFLAGS = -g
# List your sources and objects here.
SOURCES = filedemo.c
OBJS = filedemo.o
# list the name of your output program here.
EXECUTABLE = fileIOSystemCallDemo

include $(OBJS:.o=.d)

all : $(OBJS) $(EXECUTABLE)

$(EXECUTABLE) : $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(EXECUTABLE)  $(OBJS) $(LIBS)
	
%.o : %.c #Defines how to translate a single c file into an object file.
	echo compiling $<
	$(CC) $(CFLAGS) -c $<
	echo done compiling $<


%.d : %.c #Defines how to generate the dependencies for the given files.  -M gcc option generates dependencies.
	@set -e; rm -f $@; \
	$(CC) $(COMPLIANCE_FLAGS ) -M $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	 rm -f $@.$$$$

clean :   # Clean build in which everything is created from scratch.
	rm -f *.d
	rm -f *.i
	rm -f *.o
	rm -f *.s
	rm -f $(EXECUTABLE)
	
	
