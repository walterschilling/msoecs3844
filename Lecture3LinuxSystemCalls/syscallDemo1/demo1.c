
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>
	   
int main(int argc, char *argv[])
{
  pid_t tid;
  // Get the process id for this process.
  tid = syscall(SYS_gettid);
  printf("pid is %d\n", tid); 
  // Kill the process off.

  tid = syscall(SYS_tgkill, getpid(), tid);
}
