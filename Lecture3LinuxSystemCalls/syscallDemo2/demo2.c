
#define _GNU_SOURCE
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <linux/reboot.h>


#include <stdio.h>
	   
int main(int argc, char *argv[])
{
  pid_t tid;
  tid = syscall(SYS_gettid);

  printf("pid is %d\n", tid); 

  printf("Getting ready to reboot the system!");

  tid = syscall(SYS_reboot, LINUX_REBOOT_MAGIC1, LINUX_REBOOT_MAGIC2, LINUX_REBOOT_CMD_RESTART);
       }
