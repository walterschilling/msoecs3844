/*
 * threadDemo.c
 *  Created on: Sep 26, 2016
 *      Author: W Schilling
 The following program will demonstrate the operation of basic multithreading.
 In essence, multiple threads will be spawned and they will communicate with each other.
 W. Schilling
 **/
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#define NUMBER_OF_THREADS (4)
#define CHARACTER_BUFFER_LENGTH (64)
#define MAX_THREAD_NAME_LENGTH (CHARACTER_BUFFER_LENGTH + 1)
#define SLEEP_PERIOD (1)
#define MS_PER_SECOND (1000)
#define SLEEP_PERIOD_IN_MS (SLEEP_PERIOD * MS_PER_SECOND)
#define SLEEP_PERIOD_IN_US (SLEEP_PERIOD_IN_MS * 1000)

static const char *messages[] = { "Larry", "Curly", "Moe", "Conner" };

void *thread1(void *param);
void *thread2(void *param);
void *thread3(void *param);
void *thread4(void *param);

const int returnOK = EXIT_SUCCESS;

static volatile bool continueRunning = true;  // Global variable which determines if
// program should continue.

#define BUFFER_SIZE 255
static char message[BUFFER_SIZE];            // User input into program.
static char messageCopy[BUFFER_SIZE];

/**
 This is the main method.  It will handle the complete operaton fo the program.
 **/
int main(int argc, char* argv[]) {
	// Declare an array of pthread_t to store the results of threat creation.
	pthread_t tids[NUMBER_OF_THREADS];
	// Declare and array for the threading attributes.
	pthread_attr_t attr[NUMBER_OF_THREADS];
	int index;

	// Init the thread and cause it to start running.
	pthread_attr_init(&attr[0]);
	/* Create the thread. */
	pthread_create(&tids[0], &attr[0], thread1, (void*) messages[0]);

	pthread_attr_init(&attr[1]);
	/* Create the thread. */
	pthread_create(&tids[1], &attr[1], thread2, (void*) messages[1]);

	pthread_attr_init(&attr[2]);
	/* Create the thread. */
	pthread_create(&tids[2], &attr[2], thread3, (void*) messages[2]);

	pthread_attr_init(&attr[3]);
	/* Create the thread. */
	pthread_create(&tids[3], &attr[3], thread4, (void*) messages[3]);

	// Wait for each of the threads to die off.
	for (index = 0; index < 1; index++) {
		pthread_join(tids[index], NULL);
	}

	return EXIT_SUCCESS;
}

/**
 This represents the first thread.  It will read information in from the keyboard and copy it to a second buffer.
 **/
void *thread1(void *parameter) {
	while (continueRunning == true) {
		printf("Enter a message.\n");
		fgets(&message[0], BUFFER_SIZE, stdin);
		strcpy(&messageCopy[0], &message[0]); // Copy the message to the other buffer.
		if (strncmp(&message[0], "QUIT", 4) == 0) {
			continueRunning = false;
			pthread_exit(NULL);
		}
	}
	return (void*)&returnOK;
}

/**
 This method will read from the buffer and print out the buffer to the console.
 **/
void *thread2(void * parameter) {
	int index = 0;
	char threadName[MAX_THREAD_NAME_LENGTH];

	strcpy(threadName, (char*) parameter);

	while (continueRunning == true) {
		printf("%s: %d\n", threadName, index);
		if (strlen(&message[0]) > 0) {
			printf("Message received: %s\n", &message[0]);
			message[0] = 0; // Zero out the first byte to indicate there is no message there.
		}
		usleep(SLEEP_PERIOD_IN_US / 2);
		index++;
	}
	printf("Thread %s exiting\n", threadName);
	return (void*)&returnOK;
}
/**
 This method will cause a real time clock to display out to the console as the program is running.
 **/
void *thread3(void * parameter) {
	int minutes = 0;
	int seconds = 0;
	char threadName[MAX_THREAD_NAME_LENGTH];

	strcpy(threadName, (char*) parameter);

	while (continueRunning == true) {
		usleep(SLEEP_PERIOD_IN_US);
		seconds++;

		if (seconds == 60) {
			seconds = 0;
			minutes++;
		}
		printf("%d:%d\n", minutes, seconds);

	}
	printf("Thread %s exiting\n", threadName);
	return (void*)&returnOK;
}

/**
 This method will implement a thread which will print out the reverse of the given message.
 **/
void *thread4(void * parameter) {
	int index = 0;
	char threadName[MAX_THREAD_NAME_LENGTH];

	strcpy(threadName, (char*) parameter);

	printf("%s has started...\n", threadName);

	while (continueRunning == true) {
		printf("%s is running...\n", threadName);

		if (strlen(&messageCopy[0]) > 0) {
			int length;
			int idx;
			printf("%s: %d\n", threadName, index);

			printf("Message received: %s\n", &messageCopy[0]);

			length = strlen(&messageCopy[0]);
			idx = 0;

			while (idx < length / 2) {
				char tmp;

				tmp = messageCopy[idx];
				messageCopy[idx] = messageCopy[length - idx];
				messageCopy[length - idx] = tmp;
				idx++;
			}
			messageCopy[0] = '1';
			printf("Message Copy reversed : %s\n", &messageCopy[0]);

			messageCopy[0] = 0;
		}
		usleep(SLEEP_PERIOD_IN_US / 2);
		index++;
	}
	printf("Thread %s exiting\n", threadName);
	return (void*)&returnOK;
}
