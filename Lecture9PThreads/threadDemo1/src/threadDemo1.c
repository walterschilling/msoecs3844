/*
 * ThreadDemo2.c
 *
 *  Created on: Sep 26, 2016
 *      Author: W. Schilling
 *      This is a simple demo that shows how multiple threads can be spawned.
 */

#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define NUMBER_OF_THREADS (5)
#define CHARACTER_BUFFER_LENGTH (64)
#define MAX_THREAD_NAME_LENGTH (CHARACTER_BUFFER_LENGTH + 1)
#define SLEEP_PERIOD (1)
#define MS_PER_SECOND (1000)
#define US_PER_SECOND (1000000)
#define SLEEP_PERIOD_IN_MS (SLEEP_PERIOD * US_PER_SECOND)

static const char *messages[] = { "One", "Two", "Three", "Four", "Five"};
void *runner(void *param);

int main(int argc, char* argv[]) {
	// Declare a set of id's for the threads.
	pthread_t tids[NUMBER_OF_THREADS];
	pthread_attr_t attr[NUMBER_OF_THREADS];
	int index;

	for (index = 0; index < NUMBER_OF_THREADS; index++) {
		pthread_attr_init(&attr[index]);
		/* Create the thread. */
		pthread_create(&tids[index], &attr[index], runner,
				(void*) messages[index]);
	}
	for (index = 0; index < NUMBER_OF_THREADS; index++) {
		// Wait for the threads to die off.
		pthread_join(tids[index], NULL);
	}
	printf("Goodbye from the main program.");

	return 0;
}

/**
 * This is an example thread.  Note that you can pass a single pointer parameter on to the thre4ad.
 * Think of this as a run from Java multithreading.
 */
void *runner(void *parameter) {
	int count = 5;
	int index;
	char threadName[MAX_THREAD_NAME_LENGTH];

	// Copy the parameter into the name field.
	strcpy(threadName, (char*) parameter);

	for (index = 0; index < count; index++) {
		printf("%s: %d\n", threadName, index);
		usleep(SLEEP_PERIOD_IN_MS);
	}
	printf("Thread %s exiting\n", threadName);

	return NULL;
}
