#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

static void printRow(int rowNumber);


/**
 * printRow - This method will print one row of output for this program.
 * Arguments:
 *   int rowNumber - This si the number of the row within the table that is to be printed.
 **/
static void printRow(int rowNumber)
{
  int index;

  for (index = 1; index <= 10; index++)
    {
      printf("%d\t", index*rowNumber);
    }
  printf("\n");
  fflush(stdout);
}

/**
 * main - This is the main method for the program.
 * int argc - This is the number of arguments passed to the program.
 * char *argv[] - This is an array of strings representing the arguments passed in.
 **/
int main(int argc, char* argv[])
{
  int index = 0;
  pid_t allMyChildren[10];

  for (index = 1; index <= 10; index++)
  {
    pid_t pid;

    pid = fork();

    if (pid < 0)
      {
	printf("Houston!  We have a problem!.\n");
      }
    else if (pid==0)
      {
	int status;

        if (index > 1)
	  {

	    // Synchronizing the processes.
	    waitpid(allMyChildren[index-2], &status, 0);  // <- Note: Upon further review, this statement does nothing, as the wait methods only can wait on other child processes.  For this to truly work with sibling processes, we'll need to use a more advanced mechanism, which is the better way to handle this anyways.  Wait a few weeks for the right answer.
	    //usleep(20000*index); // <= This line is cheating but makes it work.
	  }

	// We are the child.
	printRow(index);
	exit(0);
      }
    else
      {
	// We are the parent.
	allMyChildren[index-1]=pid;
      }
  }
  for (index = 0; index < 10; index++)
    {
      int status;
      // Wait for all of the children to die before the parent process dies.
      waitpid(allMyChildren[index], &status, 0);

    }

}
