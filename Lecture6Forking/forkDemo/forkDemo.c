/*
 * forkDemo.c
 *
 *  Created on: Sep 18, 2016
 *      Author: W. Schilling
 *      This program demonstrates what happens when a fork occurs.  We will fork a second process and be able to trace it through as it executes.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdint.h>

int main(int argc, char* argv[]) {
	pid_t pid; /* Declare a pid type. */
	pid_t mypid; /* Declare a second pid type. */

	mypid = getpid(); /* Get my pid from the os. */
	printf("P: My PID is %d\n", mypid);

	// Fork off a second process.
	pid = fork();

	if (pid < 0) { /* An error has occurred. */
	} else if (pid > 0) { /* I am the parent process and my child is the following process. */
		printf("P: My child's pid is %d.\n", pid);
		// Wait for my child process to terminate.
		waitpid(pid, NULL, 0);
		printf("P: The child process is finished.\n");
	} else {/*I am the child process. */
		uint32_t index;
		printf("C: I am the child process.  I will do something useful.\n");
		// Print out the child pid.
		printf("C: My pid is %d.\n", getpid());
		for (index = 0; index < 10; index++) {
			printf("C: Child counting %d\n", index);
		}
		// The child will now exit.
		printf("The child is done.\n");
		exit(EXIT_SUCCESS);
	}
	// Now the parent will exit.
	printf("P: The parent is exiting.\n");
	exit(EXIT_SUCCESS);
}
