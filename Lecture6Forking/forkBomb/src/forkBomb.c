/*
 ============================================================================
 Name        : forkBomb.c
 Author      : W. Schilling
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

int main(void) {
	while (1)
	{
		pid_t mypid = getpid();

		// Randomly malloc some data.
		//void* data = malloc((mypid*2)%1000);

		pid_t childPID = fork();
		if (childPID < 0)
		{
			 printf("ERROR 1: %s\n",strerror(errno));
		}

		if (childPID > 0)
		{
			fflush(stdout);
			// I am the parent and I just forked a child.
			printf("%d just forked %d.\n", mypid, childPID);
		}
		if(childPID == 0)
		{
			usleep(10000);
		}
	}
}
